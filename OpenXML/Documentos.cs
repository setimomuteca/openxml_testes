﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace OpenXML
{
    public class Documentos
    {
        /// <summary>
        /// 
        /// </summary>
        public Documentos()
        {

        }

        private string fileName = @"C:\_Temp Clientes\Kixicredito\Desembolsos v1.4.xlsx";




        //Cria a planilha incluindo as folhas 
        public void CriaDocumento()
        {
           
            using(SpreadsheetDocument document = SpreadsheetDocument.Create(fileName, SpreadsheetDocumentType.Workbook))            
            {
                //Adiciona o livro ao ficheiro
                WorkbookPart workbookPart = document.AddWorkbookPart();

                workbookPart.Workbook = new Workbook();

                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

          
                Sheet sheet = new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "Folha de Testes"// + i
                };
                

                sheets.Append(sheet);
 
         
                workbookPart.Workbook.Save();

            }
        }

        //Insere dados na planilha 
        public void InsertData(int numLinhas, int numColunas, string texto)
        {
            using (SpreadsheetDocument documento = SpreadsheetDocument.Open(fileName, true))
            {
                WorkbookPart workbookPart = documento.WorkbookPart;
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();

                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

                for (int linha = 0; linha < numLinhas; linha++)
                {
                    Row r = new Row();

                    for (int col = 0; col < numColunas; col++)
                    {
                        Cell c = new Cell();
                        CellFormula f = new CellFormula();
                        f.CalculateCell = true;
                        f.Text = "RAND()";
                        c.Append(f);
                        CellValue v = new CellValue();
                        c.Append(v);
                        r.Append(c);

                    }
                    sheetData.Append(r);
                }
            }
        }


        //Mostra dados do ficheiro
        public void GetData( string ficheiro)
        {
            string fileName = @"C:\_Temp Clientes\Kixicredito\Desembolsos v1.4.xlsx";
            // Open the document for editing.
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();

                OpenXmlReader reader = OpenXmlReader.Create(worksheetPart);
                string text;

                while (reader.Read())
                {
                    if (reader.ElementType == typeof(CellValue))
                    {
                        text = reader.GetText();
                        Console.WriteLine(text + " ");
                    }
                }

                Console.ReadKey();
            }


        }

        

       







        

    }
}
