﻿using System;

using System.Windows.Forms;



namespace OpenXML
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Documentos doc = new Documentos();

        private void btnCreate_Click(object sender, EventArgs e)
        {
            doc.CriaDocumento();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            doc.InsertData(10000,300,"Ficheiro de testes");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            doc.GetData("");
        }
    }
}
