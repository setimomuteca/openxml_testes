﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenXmlconsole
{
    public class ColunasExcel
    {
        //Versão 1.4
        public enum Desembolso
        {
            Agencia = 1,
            LoanNumber = 2,
            CodCliente = 3,
            Cliente = 4,
            Produto = 5,
            DataDesembolso = 6,
            Montante = 7,
            Voucher = 8,
            RefBancaria = 9,
            Banco = 10,
            Conta = 11,
            TaxaProcessamento = 12,
            IVA_TP = 13,
            TaxaImprevisto = 14,
            IVA_TI = 15,
            Colateral = 16,
            Reforco = 17,
            NivelA = 18
        }

        //Versão 1.3
        public enum Reembolso
        {
            Agencia = 1,
            LoanNumber = 2,
            CodCliente = 3,
            Cliente = 4,
            Produto = 5,
            DataPagamento = 6,
            Voucher = 7,
            Principal = 8,
            Juro = 9,
            Multa = 10,
            Total = 11,
            RefBancaria = 12,
            Banco = 13,
            Conta = 14
        }

        //Versão 1.1
        public enum Poupanca
        {
            Agencia = 1,
            LoanNumber = 2,
            CodCliente = 3,
            Cliente = 4,
            Produto = 5,
            DataPagamento = 6,
            Voucher = 7,
            Principal = 8,
            Juro = 9,
            Multa = 10,
            Total = 11,
            RefBancaria = 12,
            Banco = 13,
            Conta = 14
        }


        public class DescricaoDesembolso
        {
            public readonly string A = "Agencia";
            public readonly string B = "Loan Number";
            public readonly string C = "Cod. Cliente";
            public readonly string D = "Cliente";
            public readonly string E = "Produto";
            public readonly string F = "Data de Desembolso";
            public readonly string G = "Montante";
            public readonly string H = "Voucher";
            public readonly string I = "Ref. Bancária";
            public readonly string J = "Banco";
            public readonly string K = "NroConta";
            public readonly string L = "TaxaProcessamento";
            public readonly string M = "IVA_TP";
            public readonly string N = "TaxaImprevisto";
            public readonly string O = "IVA_TI";
            public readonly string P = "Colateral Deduzido";
            public readonly string Q = "Reforço";
            public readonly string R = "NivelA(Carteira)";
        }

        public class DescricaoReembolso
        {
            public readonly string A = "Agencia";
            public readonly string B = "Loan Number";
            public readonly string C = "Cod. Cliente";
            public readonly string D = "Cliente";
            public readonly string E = "Produto";
            public readonly string F = "Data Pagamento";
            public readonly string G = "Voucher";
            public readonly string H = "Principal";
            public readonly string I = "Juro";
            public readonly string J = "Multa";
            public readonly string K = "Total";
            public readonly string L = "Ref. Bancária";
            public readonly string M = "Banco";
            public readonly string N = "Conta";
        }

        public class DescricaoPoupanca
        {
            public readonly string A = "Agencia";
            public readonly string B = "Loan Number";
            public readonly string C = "Cod. Cliente";
            public readonly string D = "Cliente";
            public readonly string E = "Produto";
            public readonly string F = "Data Pagamento";
            public readonly string G = "Voucher";
            public readonly string H = "Principal";
            public readonly string I = "Juro";
            public readonly string J = "Multa";
            public readonly string K = "Total";
            public readonly string L = "Ref. Bancária";
            public readonly string M = "Banco";
            public readonly string N = "Conta";
        }
    }
}
