﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using OpenXmlconsole;

namespace OpenXMLconsole
{
    public class Documento
    {
        public Documento(){}

        private SpreadsheetDocument documento;
        private WorkbookPart workBook;
        private WorksheetPart workSheet;
        private Sheet sheet;
        private string strErros = "";


        public void GetData()
        {
            const string fileName = @"C:\_Temp Clientes\Kixicredito\Book1.xlsx";
            //const string sheetName = "FolhaTeste";
            const string sheetName = "Teste2";
            string col_A = "";
            string col_B = "";
            string col_C = "";
            string col_D = "";
            string col_E = "";
            string valor_A = "";
            int valor_B = 0;
            DateTime valor_C;
            double valor_D = 0.00;
            bool valor_E = false;
            int numLinhas = 0;
            StringBuilder tempErros = new StringBuilder();

            documento = SpreadsheetDocument.Open(fileName, false);
            
            Console.WriteLine(DateTime.Now.ToString("hh:mm:ss") + " Inicio: A abrir o ficheiro: " + fileName);
            Console.WriteLine("Workbook");
            //Set WorkBook
            workBook = documento.WorkbookPart;

            Console.WriteLine("Sheet");
            //Set Sheet
            sheet = workBook.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetName).FirstOrDefault();

            if (sheet == null){throw new ArgumentException(sheetName);}

            Console.WriteLine("workSheet");
            //Set WorkSheet
            workSheet = (WorksheetPart)(workBook.GetPartById(sheet.Id));

            Console.WriteLine(DateTime.Now.ToString("hh:mm:ss") + " GetRowNumber");
            numLinhas = GetRowNumber(workSheet);

            Console.WriteLine(DateTime.Now.ToString("hh:mm:ss") + " numLinhas:" + numLinhas.ToString() + " ");

            for (int i = 2; i <= numLinhas; i++)
            {
                col_A = "A" + i.ToString(); //String
                col_B = "B" + i.ToString(); //Int
                col_C = "C" + i.ToString(); //Date
                col_D = "D" + i.ToString(); //Double
                col_E = "E" + i.ToString(); //Bool

                tempErros.Clear();

                valor_A = GetCellValue(col_A) ?? "";
                if (strErros.Length > 0) { tempErros.Append(strErros); }
                valor_B = Convert.ToInt32(GetCellValue(col_B) ?? "0");
                if (strErros.Length > 0) { tempErros.Append(strErros); }
                valor_C = DateTime.FromOADate(Convert.ToDouble(GetCellValue(col_C) ?? "2")); //1900-01-01
                if (strErros.Length > 0) { tempErros.Append(strErros); }
                valor_D = Convert.ToDouble(GetCellValue(col_D) ?? "0");
                if (strErros.Length > 0) { tempErros.Append(strErros); }
                valor_E = Convert.ToBoolean((Convert.ToByte(GetCellValue(col_E) ?? "0")));
                if (strErros.Length > 0) { tempErros.Append(strErros); }

                Console.WriteLine(
                    valor_A + " "
                    + valor_B.ToString() + " "
                    + valor_C.ToString("yyyy-MM-dd") + " " 
                    + valor_D.ToString() + " " 
                    + valor_E.ToString()
                    );

                if (tempErros.Length > 0) { Console.WriteLine(tempErros); }
            }

            Console.WriteLine(DateTime.Now.ToString("hh:mm:ss") + " Fim");
            Console.ReadKey();
            
            
        }

        public int GetRowNumber(WorksheetPart ws)
        {
            int resultado = 0;

            //find sheet data
            IEnumerable<SheetData> sheetData = ws.Worksheet.Elements<SheetData>();

            // Iterate through every sheet inside Excel sheet
            foreach (SheetData SD in sheetData)
            {
                IEnumerable<Row> row = SD.Elements<Row>(); // Get the row IEnumerator
                resultado = row.Count(); // Will give you the count of rows
            }

            return resultado;
        }

        // Retrieve the value of a cell, given a file name, sheet name, 
        // and address name.
        public string GetCellValue(string addressName)
        {
            string value = null;

            strErros = "";

            // Use its Worksheet property to get a reference to the cell 
            // whose address matches the address you supplied.
            Cell theCell = workSheet.Worksheet.Descendants<Cell>().
                Where(c => c.CellReference == addressName).FirstOrDefault();

            try
            {
                if(theCell == null) { throw new Exception("Célula: " + addressName + " vazia."); }

                // If the cell does not exist, return an empty string.
                if (theCell.InnerText.Length > 0)
                {
                    value = theCell.InnerText;

                    // If the cell represents an integer number, you are done. 
                    // For dates, this code returns the serialized value that 
                    // represents the date. The code handles strings and 
                    // Booleans individually. For shared strings, the code 
                    // looks up the corresponding value in the shared string 
                    // table. For Booleans, the code converts the value into 
                    // the words TRUE or FALSE.
                    if (theCell.DataType != null)
                    {
                        switch (theCell.DataType.Value)
                        {
                            case CellValues.SharedString:

                                // For shared strings, look up the value in the
                                // shared strings table.
                                var stringTable =
                                    workBook.GetPartsOfType<SharedStringTablePart>()
                                    .FirstOrDefault();

                                // If the shared string table is missing, something 
                                // is wrong. Return the index that is in
                                // the cell. Otherwise, look up the correct text in 
                                // the table.
                                if (stringTable != null)
                                {
                                    value =
                                        stringTable.SharedStringTable
                                        .ElementAt(int.Parse(value)).InnerText;
                                }
                                break;

                            case CellValues.Boolean:
                                switch (value)
                                {
                                    case "0":
                                        value = "FALSE";
                                        break;
                                    default:
                                        value = "TRUE";
                                        break;
                                }
                                break;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                strErros = ex.Message + " ";
                value = null;
            }

            return value;
        }
    }
    
}

